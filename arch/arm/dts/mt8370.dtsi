// SPDX-License-Identifier: (GPL-2.0 OR MIT)
/*
 * Copyright (C) 2023 MediaTek Inc.
 * Author: Macpaul Lin <macpaul.lin@mediatek.com>
 */

#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/clock/mt8188-clk.h>
#include <dt-bindings/interrupt-controller/irq.h>
#include <dt-bindings/interrupt-controller/arm-gic.h>
#include <dt-bindings/pinctrl/mediatek,mt8188-pinfunc.h>
#include <dt-bindings/power/mt8188-power.h>
#include <dt-bindings/phy/phy.h>

/ {
	compatible = "mediatek,mt8370";
	interrupt-parent = <&sysirq>;
	#address-cells = <2>;
	#size-cells = <2>;

	aliases {
		serial0 = &uart0;
		i2c0 = &i2c0;
		i2c1 = &i2c1;
		i2c2 = &i2c2;
		i2c3 = &i2c3;
		i2c4 = &i2c4;
		i2c5 = &i2c5;
		i2c6 = &i2c6;
	};

	cpus {
		#address-cells = <1>;
		#size-cells = <0>;

		cpu-map {
			cluster0 {
				core0 {
					cpu = <&cpu0>;
				};
				core1 {
					cpu = <&cpu1>;
				};
				core2 {
					cpu = <&cpu2>;
				};
				core3 {
					cpu = <&cpu3>;
				};
			};
			cluster1 {
				core0 {
					cpu = <&cpu4>;
				};
				core1 {
					cpu = <&cpu5>;
				};
			};
		};

		cpu0: cpu@0 {
			device_type = "cpu";
			compatible = "arm,cortex-a55", "arm,armv8";
			reg = <0x000>;
			enable-method = "psci";
			capacity-dmips-mhz = <282>;
		};

		cpu1: cpu@1 {
			device_type = "cpu";
			compatible = "arm,cortex-a55", "arm,armv8";
			reg = <0x100>;
			enable-method = "psci";
			capacity-dmips-mhz = <282>;
		};

		cpu2: cpu@2 {
			device_type = "cpu";
			compatible = "arm,cortex-a55", "arm,armv8";
			reg = <0x200>;
			enable-method = "psci";
			capacity-dmips-mhz = <282>;
		};

		cpu3: cpu@3 {
			device_type = "cpu";
			compatible = "arm,cortex-a55", "arm,armv8";
			reg = <0x300>;
			enable-method = "psci";
			capacity-dmips-mhz = <282>;
		};

		cpu4: cpu@100 {
			device_type = "cpu";
			compatible = "arm,cortex-a78", "arm,armv8";
			reg = <0x600>;
			enable-method = "psci";
			capacity-dmips-mhz = <1024>;
		};

		cpu5: cpu@101 {
			device_type = "cpu";
			compatible = "arm,cortex-a78", "arm,armv8";
			reg = <0x700>;
			enable-method = "psci";
			capacity-dmips-mhz = <1024>;
		};
	};

	clk26m: oscillator {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <26000000>;
		clock-output-names = "clk26m";
	};

	mmc_source_clk: mmc-source-clk{
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <400000000>;
		clock-output-names = "mmc_source_clk";
	};

	soc {
		#address-cells = <2>;
		#size-cells = <2>;
		compatible = "simple-bus";
		ranges;

		watchdog: watchdog@10007000 {
			compatible = "mediatek,mt8195-wdt",
				      "mediatek,wdt";
			reg = <0 0x10007000 0 0x100>;
			status = "disabled";
		};

		gic: interrupt-controller@c000000 {
			compatible = "arm,gic-v3";
			#interrupt-cells = <4>;
			interrupt-parent = <&gic>;
			interrupt-controller;
			reg = <0 0x0c000000 0 0x40000>,  /* GICD */
			      <0 0x0c100000 0 0x200000>, /* GICR */
			      <0 0x0c400000 0 0x2000>,   /* GICC */
			      <0 0x0c410000 0 0x1000>,   /* GICH */
			      <0 0x0c420000 0 0x2000>;   /* GICV */

			interrupts = <GIC_PPI 9 IRQ_TYPE_LEVEL_HIGH 0>;
			ppi-partitions {
				ppi_cluster0: interrupt-partition-0 {
					affinity = <&cpu0 &cpu1 &cpu2 &cpu3>;
				};
				ppi_cluster1: interrupt-partition-1 {
					affinity = <&cpu4 &cpu5>;
				};
			};
		};

		sysirq: interrupt-controller@c530a80 {
			compatible = "mediatek,mt8195-sysirq",
				     "mediatek,mt6577-sysirq";
			interrupt-controller;
			#interrupt-cells = <3>;
			interrupt-parent = <&gic>;
			reg = <0 0x0c530a80 0 0x50>;
		};

		topckgen: topckgen@10000000 {
			compatible = "mediatek,mt8188-topckgen";
			reg = <0 0x10000000 0 0x1000>;
			#clock-cells = <1>;
		};

		topckgen_cg: topckgen-cg@10000000 {
			compatible = "mediatek,mt8188-topckgen-cg";
			reg = <0 0x10000000 0 0x1000>;
			#clock-cells = <1>;
		};

		infracfg_ao: infracfg-ao@10001000 {
			compatible = "mediatek,mt8188-infracfg_ao", "syscon";
			reg = <0 0x10001000 0 0x1000>;
			#clock-cells = <1>;
		};

		pericfg_ao: pericfg-ao@11003000 {
			compatible = "mediatek,mt8188-pericfg_ao";
			reg = <0 0x11003000 0 0x1000>;
			#clock-cells = <1>;
		};

		apmixedsys: apmixedsys@1000c000 {
			compatible = "mediatek,mt8188-apmixedsys";
			reg = <0 0x1000c000 0 0x1000>;
			#clock-cells = <1>;
		};

		pio: pinctrl@10005000 {
			compatible = "mediatek,mt8188-pinctrl";
			reg = <0 0x10005000 0 0x1000>,
			<0 0x11c00000 0 0x1000>,
			<0 0x11e10000 0 0x1000>,
			<0 0x11e20000 0 0x1000>,
			<0 0x11ea0000 0 0x1000>,
			<0 0x1000b000 0 0x1000>;
			reg-names = "iocfg0", "iocfg_rm", "iocfg_lt",
				    "iocfg_lm", "iocfg_rt", "eint";
			gpio: gpio-controller {
				gpio-controller;
				#gpio-cells = <2>;
			};
		};

		spm: power-controller@10006000 {
			compatible = "mediatek,mt8188-scpsys", "syscon";
			reg = <0 0x10006000 0 0x1000>;
			#address-cells = <1>;
			#size-cells = <0>;
			#power-domain-cells = <1>;
			infracfg = <&infracfg_ao>;

			mfg0@MT8188_POWER_DOMAIN_MFG0 {
				reg = <MT8188_POWER_DOMAIN_MFG0>;
				#address-cells = <1>;
				#size-cells = <0>;
				#power-domain-cells = <1>;
			};

			vppsys0@MT8188_POWER_DOMAIN_VPPSYS0 {
				reg = <MT8188_POWER_DOMAIN_VPPSYS0>;
				clocks = <&topckgen CLK_TOP_VPP>,
					 <&topckgen CLK_TOP_CAM>,
					 <&topckgen CLK_TOP_CCU>,
					 <&topckgen CLK_TOP_IMG>,
					 <&topckgen CLK_TOP_VENC>,
					 <&topckgen CLK_TOP_VDEC>,
					 <&topckgen CLK_TOP_WPE_VPP>,
					 <&topckgen CLK_TOP_CFGREG_CLOCK_EN_VPP0>,
					 <&topckgen CLK_TOP_CFGREG_F26M_VPP0>,
					 <&vppsys0 CLK_VPP0_SMI_COMMON_MMSRAM>,
					 <&vppsys0 CLK_VPP0_GALS_VDO0_LARB0_MMSRAM>,
					 <&vppsys0 CLK_VPP0_GALS_VDO0_LARB1_MMSRAM>,
					 <&vppsys0 CLK_VPP0_GALS_VENCSYS_MMSRAM>,
					 <&vppsys0 CLK_VPP0_GALS_VENCSYS_CORE1_MMSRAM>,
					 <&vppsys0 CLK_VPP0_GALS_INFRA_MMSRAM>,
					 <&vppsys0 CLK_VPP0_GALS_CAMSYS_MMSRAM>,
					 <&vppsys0 CLK_VPP0_GALS_VPP1_LARB5_MMSRAM>,
					 <&vppsys0 CLK_VPP0_GALS_VPP1_LARB6_MMSRAM>,
					 <&vppsys0 CLK_VPP0_SMI_REORDER_MMSRAM>,
					 <&vppsys0 CLK_VPP0_SMI_IOMMU>,
					 <&vppsys0 CLK_VPP0_GALS_IMGSYS_CAMSYS>,
					 <&vppsys0 CLK_VPP0_GALS_EMI0_EMI1>,
					 <&vppsys0 CLK_VPP0_SMI_SUB_COMMON_REORDER>,
					 <&vppsys0 CLK_VPP0_SMI_RSI>,
					 <&vppsys0 CLK_VPP0_SMI_COMMON_LARB4>,
					 <&vppsys0 CLK_VPP0_GALS_VDEC_VDEC_CORE1>,
					 <&vppsys0 CLK_VPP0_GALS_VPP1_WPESYS>,
					 <&vppsys0 CLK_VPP0_GALS_VDO0_VDO1_VENCSYS_CORE1>;
				clock-names = "vppsys00", "vppsys01", "vppsys02", "vppsys03",
					      "vppsys04", "vppsys05", "vppsys06", "vppsys07",
					      "vppsys08",
					      "vppsys0-0", "vppsys0-1", "vppsys0-2", "vppsys0-3",
					      "vppsys0-4", "vppsys0-5", "vppsys0-6", "vppsys0-7",
					      "vppsys0-8", "vppsys0-9", "vppsys0-10", "vppsys0-11",
					      "vppsys0-12", "vppsys0-13", "vppsys0-14",
					      "vppsys0-15", "vppsys0-16", "vppsys0-17",
					      "vppsys0-18";
				infracfg = <&infracfg_ao>;
				#address-cells = <1>;
				#size-cells = <0>;
				#power-domain-cells = <1>;

				vdosys0@MT8188_POWER_DOMAIN_VDOSYS0 {
					reg = <MT8188_POWER_DOMAIN_VDOSYS0>;
					clocks = <&topckgen CLK_TOP_CFGREG_CLOCK_EN_VDO0>,
						 <&topckgen CLK_TOP_CFGREG_F26M_VDO0>,
						 <&vdosys0 CLK_VDO0_SMI_GALS>,
						 <&vdosys0 CLK_VDO0_SMI_COMMON>,
						 <&vdosys0 CLK_VDO0_SMI_EMI>,
						 <&vdosys0 CLK_VDO0_SMI_IOMMU>,
						 <&vdosys0 CLK_VDO0_SMI_LARB>,
						 <&vdosys0 CLK_VDO0_SMI_RSI>,
						 <&vdosys0 CLK_VDO0_APB_BUS>;
					clock-names = "vdosys00", "vdosys01",
						      "vdosys0-0", "vdosys0-1", "vdosys0-2",
						      "vdosys0-3", "vdosys0-4", "vdosys0-5",
						      "vdosys0-6";
					infracfg = <&infracfg_ao>;
					#address-cells = <1>;
					#size-cells = <0>;
					#power-domain-cells = <1>;

					vppsys1@MT8188_POWER_DOMAIN_VPPSYS1 {
						reg = <MT8188_POWER_DOMAIN_VPPSYS1>;
						clocks = <&topckgen CLK_TOP_CFGREG_CLOCK_EN_VPP1>,
							 <&topckgen CLK_TOP_CFGREG_F26M_VPP1>,
							 <&vppsys1 CLK_VPP1_GALS5>,
							 <&vppsys1 CLK_VPP1_GALS6>,
							 <&vppsys1 CLK_VPP1_LARB5>,
							 <&vppsys1 CLK_VPP1_LARB6>;
						clock-names = "vppsys10", "vppsys11",
							      "vppsys1-0", "vppsys1-1",
							      "vppsys1-2", "vppsys1-3";
						infracfg = <&infracfg_ao>;
						#power-domain-cells = <0>;
					};

					vdosys1@MT8188_POWER_DOMAIN_VDOSYS1 {
						reg = <MT8188_POWER_DOMAIN_VDOSYS1>;
						clocks = <&topckgen CLK_TOP_CFGREG_CLOCK_EN_VDO1>,
							 <&topckgen CLK_TOP_CFGREG_F26M_VDO1>,
							 <&vdosys1 CLK_VDO1_SMI_LARB2>,
							 <&vdosys1 CLK_VDO1_SMI_LARB3>,
							 <&vdosys1 CLK_VDO1_GALS>;
						clock-names = "vdosys10", "vdosys11",
							      "vdosys1-0", "vdosys1-1", "vdosys1-2";
						infracfg = <&infracfg_ao>;
						#address-cells = <1>;
						#size-cells = <0>;
						#power-domain-cells = <1>;

						hdmi-tx@MT8188_POWER_DOMAIN_HDMI_TX {
							reg = <MT8188_POWER_DOMAIN_HDMI_TX>;
							clocks = <&topckgen CLK_TOP_HDMI_APB>,
								 <&topckgen CLK_TOP_HDCP_24M>;
							clock-names = "hdmi_tx0", "hdmi_tx1";
							infracfg = <&infracfg_ao>;
							#power-domain-cells = <0>;
						};

						dp-tx@MT8188_POWER_DOMAIN_DP_TX {
							reg = <MT8188_POWER_DOMAIN_DP_TX>;
							infracfg = <&infracfg_ao>;
							#power-domain-cells = <0>;
						};

						edp-tx@MT8188_POWER_DOMAIN_EDP_TX {
							reg = <MT8188_POWER_DOMAIN_EDP_TX>;
							infracfg = <&infracfg_ao>;
							#power-domain-cells = <0>;
						};
					};
				};
			};

			pextp-mac-p0@MT8188_POWER_DOMAIN_PEXTP_MAC_P0 {
				reg = <MT8188_POWER_DOMAIN_PEXTP_MAC_P0>;
				infracfg = <&infracfg_ao>;
				clocks = <&pericfg_ao CLK_PERI_AO_PCIE_P0_FMEM>;
				clock-names = "pextp_mac_p0-0";
				#power-domain-cells = <0>;
			};

			csirx-top@MT8188_POWER_DOMAIN_CSIRX_TOP {
				reg = <MT8188_POWER_DOMAIN_CSIRX_TOP>;
				clocks = <&topckgen CLK_TOP_SENINF>,
					 <&topckgen CLK_TOP_SENINF1>;
				clock-names = "csirx_top0", "csirx_top1";
				#power-domain-cells = <0>;
			};

			pextp-phy-top@MT8188_POWER_DOMAIN_PEXTP_PHY_TOP {
				reg = <MT8188_POWER_DOMAIN_PEXTP_PHY_TOP>;
				#power-domain-cells = <0>;
			};

			adsp-ao@MT8188_POWER_DOMAIN_ADSP_AO {
				reg = <MT8188_POWER_DOMAIN_ADSP_AO>;
				clocks = <&topckgen CLK_TOP_AUDIO_LOCAL_BUS>,
					 <&topckgen CLK_TOP_ADSP>;
				clock-names = "adsp_ao0", "adsp_ao1";
				infracfg = <&infracfg_ao>;
				#address-cells = <1>;
				#size-cells = <0>;
				#power-domain-cells = <1>;

				adsp-infra@MT8188_POWER_DOMAIN_ADSP_INFRA {
					reg = <MT8188_POWER_DOMAIN_ADSP_INFRA>;
					infracfg = <&infracfg_ao>;
					#address-cells = <1>;
					#size-cells = <0>;
					#power-domain-cells = <1>;

					audio-asrc@MT8188_POWER_DOMAIN_AUDIO_ASRC {
						reg = <MT8188_POWER_DOMAIN_AUDIO_ASRC>;
						clocks = <&topckgen CLK_TOP_ASM_H>;
						clock-names = "audio_asrc0";
						infracfg = <&infracfg_ao>;
						#power-domain-cells = <0>;
					};

					audio@MT8188_POWER_DOMAIN_AUDIO {
						reg = <MT8188_POWER_DOMAIN_AUDIO>;
						clocks = <&topckgen CLK_TOP_A1SYS_HP>,
							 <&topckgen CLK_TOP_AUD_INTBUS>;
						clock-names = "audio0", "audio1";
						infracfg = <&infracfg_ao>;
						#power-domain-cells = <0>;
					};

					adsp@MT8188_POWER_DOMAIN_ADSP {
						reg = <MT8188_POWER_DOMAIN_ADSP>;
						infracfg = <&infracfg_ao>;
						#power-domain-cells = <0>;
					};
				};
			};

			ether@MT8188_POWER_DOMAIN_ETHER {
				reg = <MT8188_POWER_DOMAIN_ETHER>;
				clocks = <&pericfg_ao CLK_PERI_AO_ETHERNET_MAC>;
				clock-names = "ether0";
				infracfg = <&infracfg_ao>;
				#power-domain-cells = <0>;
			};
		};

		uart0: serial@11001100 {
			compatible = "mediatek,mt8195-uart",
				     "mediatek,hsuart";
			reg = <0 0x11001100 0 0x1000>;
			interrupts = <GIC_SPI 91 IRQ_TYPE_LEVEL_LOW>;
			clock-frequency = <26000000>;
			clocks = <&clk26m>, <&clk26m>;
			clock-names = "baud", "bus";
			status = "disabled";
		};

		mmc0: mmc@11230000 {
			compatible = "mediatek,mt8195-mmc",
				     "mediatek,mt8183-mmc";
			reg = <0 0x11230000 0 0x1000>,
			      <0 0x11f50000 0 0x1000>;
			interrupts = <GIC_SPI 77 IRQ_TYPE_LEVEL_LOW>;
			clocks = <&mmc_source_clk>,
				 <&clk26m>,
				 <&clk26m>;
			clock-names = "source", "hclk", "source_cg";
			status = "disabled";
		};

		mmc1: mmc@11240000 {
			compatible = "mediatek,mt8195-mmc",
				     "mediatek,mt8183-mmc";
			reg = <0 0x11240000 0 0x1000>,
			      <0 0x11eb0000 0 0x1000>;
			interrupts = <GIC_SPI 135 IRQ_TYPE_LEVEL_LOW>;
			clocks = <&mmc_source_clk>,
				 <&clk26m>,
				 <&clk26m>;
			clock-names = "source", "hclk", "source_cg";
			status = "disabled";
		};

		snor: snor@1132c000 {
			compatible = "mediatek,mt8188-nor";
			reg = <0 0x1132c000 0 0x1000>;
			interrupts = <GIC_SPI 825 IRQ_TYPE_LEVEL_HIGH 0>;
			clocks = <&topckgen CLK_TOP_SPINOR>,
					 <&pericfg_ao  CLK_PERI_AO_FLASHIFLASHCK>,
					 <&pericfg_ao  CLK_PERI_AO_FLASHIF_BUS>;
			clock-names = "spi", "sf", "axi";
			#address-cells = <1>;
			#size-cells = <0>;
			status = "disabled";
		};

		i2c0: i2c@11280000 {
			compatible = "mediatek,mt8188-i2c";
			reg = <0 0x11280000 0 0x1000>,
			      <0 0x10220080 0 0x80>;
			interrupts = <GIC_SPI 151 IRQ_TYPE_LEVEL_HIGH 0>;
			clock-div = <1>;
			clocks = <&imp_iic_wrap_c CLK_IMP_IIC_WRAP_C_AP_CLOCK_I2C0>,
				 <&infracfg_ao CLK_INFRA_AO_APDMA_BCLK>;
			clock-names = "main", "dma";
			#address-cells = <1>;
			#size-cells = <0>;
			status = "disabled";
		};

		xhci1: xhci1@11200000 {
			compatible = "mediatek,mt8188-xhci", "mediatek,mtk-xhci";
			reg = <0 0x11200000 0 0x1000>,
			      <0 0x11203e00 0 0x0100>;
			reg-names = "mac", "ippc";
			interrupt-names = "host", "wakeup";
			interrupts = <GIC_SPI 128 IRQ_TYPE_LEVEL_HIGH 0>;
			phys = <&u2port1 PHY_TYPE_USB2>, <&u3port1 PHY_TYPE_USB3>;
			clocks = <&pericfg_ao CLK_PERI_AO_SSUSB_XHCI>;
			clock-names = "sys_ck";
			status = "disabled";
		};

		xhci2: xhci2@112a0000 {
			compatible = "mediatek,mt8188-xhci", "mediatek,mtk-xhci";
			reg = <0 0x112a0000 0 0x1000>,
			      <0 0x112a3e00 0 0x0100>;
			reg-names = "mac", "ippc";
			interrupt-names = "host", "wakeup";
			interrupts = <GIC_SPI 535 IRQ_TYPE_LEVEL_HIGH 0>;
			phys = <&u2port2 PHY_TYPE_USB2>;
			clocks = <&pericfg_ao CLK_PERI_AO_SSUSB_3P_XHCI>;
			clock-names = "sys_ck";
			status = "disabled";
		};

		usb: usb@112b0000 {
			compatible = "mediatek,mt8188-mtu3", "mediatek,mtu3";
			reg = <0 0x112b0000 0 0x2dff>,
			      <0 0x112b3e00 0 0x0100>;
			reg-names = "mac", "ippc";
			phys = <&u2port0 PHY_TYPE_USB2>;
			clocks = <&pericfg_ao CLK_PERI_AO_SSUSB_2P_BUS>,
				 <&topckgen CLK_TOP_SSUSB_TOP_P2_REF>,
				 <&pericfg_ao CLK_PERI_AO_SSUSB_2P_XHCI>,
				 <&topckgen CLK_TOP_UNIVPLL_192M_D4>;
			clock-names = "sys_ck", "ref_ck", "mcu_ck", "dma_ck";
			#address-cells = <2>;
			#size-cells = <2>;
			ranges;
			status = "disabled";

			ssusb: ssusb@112b0000 {
				compatible = "mediatek,ssusb";
				reg = <0 0x112b0000 0 0x3e00>,
				      <0 0x112b3e00 0 0x0100>;
				reg-names = "mac", "ippc";
				interrupts = <GIC_SPI 532 IRQ_TYPE_LEVEL_HIGH 0>;
				status = "disabled";
			};
		};

		eth: ethernet@11021000 {
			compatible = "mediatek,mt8195-gmac", "snps,dwmac-5.10a";
			reg = <0 0x11021000 0 0x4000>;
			interrupts = <GIC_SPI 716 IRQ_TYPE_LEVEL_HIGH 0>;
			interrupt-names = "macirq";
			clock-names = "axi",
				      "apb",
				      "mac_cg",
				      "mac_main",
				      "ptp_ref",
				      "rmii_internal";
			clocks = <&pericfg_ao CLK_PERI_AO_ETHERNET>,
				 <&pericfg_ao CLK_PERI_AO_ETHERNET_BUS>,
				 <&pericfg_ao CLK_PERI_AO_ETHERNET_MAC>,
				 <&topckgen CLK_TOP_SNPS_ETH_250M>,
				 <&topckgen CLK_TOP_SNPS_ETH_62P4M_PTP>,
				 <&topckgen CLK_TOP_SNPS_ETH_50M_RMII>;
			assigned-clocks = <&topckgen CLK_TOP_SNPS_ETH_250M>,
					  <&topckgen CLK_TOP_SNPS_ETH_62P4M_PTP>,
					  <&topckgen CLK_TOP_SNPS_ETH_50M_RMII>;
			assigned-clock-parents = <&topckgen CLK_TOP_ETHPLL_D2>,
						 <&topckgen CLK_TOP_ETHPLL_D8>,
						 <&topckgen CLK_TOP_ETHPLL_D10>;
			power-domains = <&spm MT8188_POWER_DOMAIN_ETHER>;
			mediatek,pericfg = <&infracfg_ao>;
			status = "okay";
		};

		i2c1: i2c@11e00000 {
			compatible = "mediatek,mt8188-i2c";
			reg = <0 0x11e00000 0 0x1000>,
			      <0 0x10220100 0 0x80>;
			interrupts = <GIC_SPI 152 IRQ_TYPE_LEVEL_HIGH 0>;
			clock-div = <1>;
			clocks = <&imp_iic_wrap_w CLK_IMP_IIC_WRAP_W_AP_CLOCK_I2C1>,
				 <&infracfg_ao CLK_INFRA_AO_APDMA_BCLK>;
			clock-names = "main", "dma";
			#address-cells = <1>;
			#size-cells = <0>;
			status = "disabled";
		};

		i2c2: i2c@11281000 {
			compatible = "mediatek,mt8188-i2c";
			reg = <0 0x11281000 0 0x1000>,
			      <0 0x10220180 0 0x80>;
			interrupts = <GIC_SPI 146 IRQ_TYPE_LEVEL_HIGH 0>;
			clock-div = <1>;
			clocks = <&imp_iic_wrap_c CLK_IMP_IIC_WRAP_C_AP_CLOCK_I2C2>,
				 <&infracfg_ao CLK_INFRA_AO_APDMA_BCLK>;
			clock-names = "main", "dma";
			#address-cells = <1>;
			#size-cells = <0>;
			status = "disabled";
		};

		i2c3: i2c@11282000 {
			compatible = "mediatek,mt8188-i2c";
			reg = <0 0x11282000 0 0x1000>,
			      <0 0x10220280 0 0x80>;
			interrupts = <GIC_SPI 147 IRQ_TYPE_LEVEL_HIGH 0>;
			clock-div = <1>;
			clocks = <&imp_iic_wrap_c CLK_IMP_IIC_WRAP_C_AP_CLOCK_I2C3>,
				 <&infracfg_ao CLK_INFRA_AO_APDMA_BCLK>;
			clock-names = "main", "dma";
			#address-cells = <1>;
			#size-cells = <0>;
			status = "disabled";
		};

		i2c4: i2c@11e01000 {
			compatible = "mediatek,mt8188-i2c";
			reg = <0 0x11e01000 0 0x1000>,
			      <0 0x10220380 0 0x80>;
			interrupts = <GIC_SPI 148 IRQ_TYPE_LEVEL_HIGH 0>;
			clock-div = <1>;
			clocks = <&imp_iic_wrap_w CLK_IMP_IIC_WRAP_W_AP_CLOCK_I2C4>,
				 <&infracfg_ao CLK_INFRA_AO_APDMA_BCLK>;
			clock-names = "main", "dma";
			#address-cells = <1>;
			#size-cells = <0>;
			status = "disabled";
		};

		u3phy0: usb-phy0@11e30000 {
			compatible = "mediatek,generic-tphy-v2";
			#address-cells = <2>;
			#size-cells = <2>;
			ranges;
			status = "disabled";

			u2port0: usb2-phy0@11e30000 {
				reg = <0 0x11e30000 0 0x700>;
				clocks = <&topckgen CLK_TOP_SSUSB_PHY_P2_REF>,
					 <&apmixedsys CLK_APMIXED_PLL_SSUSB26M_EN>;
				clock-names = "ref", "da_ref";
				#phy-cells = <1>;
				status = "disabled";
			};
		};

		u3phy1: usb-phy1@11e40000 {
			compatible = "mediatek,generic-tphy-v2";
			#address-cells = <2>;
			#size-cells = <2>;
			ranges;
			status = "disabled";

			u2port1: usb2-phy1@11e40000 {
				reg = <0 0x11e40000 0 0x700>;
				clocks = <&topckgen CLK_TOP_SSUSB_PHY_REF>,
					 <&apmixedsys CLK_APMIXED_PLL_SSUSB26M_EN>;
				clock-names = "ref", "da_ref";
				#phy-cells = <1>;
				status = "disabled";
			};

			u3port1: usb3-phy1@11e40700 {
				reg = <0 0x11e40700 0 0x700>;
				clocks = <&apmixedsys CLK_APMIXED_PLL_SSUSB26M_EN>, <&clk26m>;
				clock-names = "ref", "da_ref";
				#phy-cells = <1>;
				status = "disabled";
			};
		};

		u3phy2: usb-phy2@11e80000 {
			compatible = "mediatek,generic-tphy-v2";
			#address-cells = <2>;
			#size-cells = <2>;
			ranges;
			status = "disabled";

			u2port2: usb2-phy2@11e80000 {
				reg = <0 0x11e80000 0 0x700>;
				clocks = <&topckgen CLK_TOP_SSUSB_PHY_P3_REF>,
					 <&apmixedsys CLK_APMIXED_PLL_SSUSB26M_EN>;
				clock-names = "ref", "da_ref";
				#phy-cells = <1>;
				status = "disabled";
			};
		};

		i2c5: i2c@11ec0000 {
			compatible = "mediatek,mt8188-i2c";
			reg = <0 0x11ec0000 0 0x1000>,
			      <0 0x10220480 0 0x80>;
			interrupts = <GIC_SPI 149 IRQ_TYPE_LEVEL_HIGH 0>;
			clock-div = <1>;
			clocks = <&imp_iic_wrap_en CLK_IMP_IIC_WRAP_EN_AP_CLOCK_I2C5>,
				 <&infracfg_ao CLK_INFRA_AO_APDMA_BCLK>;
			clock-names = "main", "dma";
			#address-cells = <1>;
			#size-cells = <0>;
			status = "disabled";
		};

		i2c6: i2c@11ec1000 {
			compatible = "mediatek,mt8188-i2c";
			reg = <0 0x11ec1000 0 0x1000>,
			      <0 0x10220600 0 0x80>;
			interrupts = <GIC_SPI 150 IRQ_TYPE_LEVEL_HIGH 0>;
			clock-div = <1>;
			clocks = <&imp_iic_wrap_en CLK_IMP_IIC_WRAP_EN_AP_CLOCK_I2C6>,
				 <&infracfg_ao CLK_INFRA_AO_APDMA_BCLK>;
			clock-names = "main", "dma";
			#address-cells = <1>;
			#size-cells = <0>;
			status = "disabled";
		};

		imp_iic_wrap_c: syscon@11283000 {
			compatible = "mediatek,mt8188-imp_iic_wrap_c";
			reg = <0 0x11283000 0 0x1000>;
			#clock-cells = <1>;
		};

		imp_iic_wrap_w: syscon@11e02000 {
			compatible = "mediatek,mt8188-imp_iic_wrap_w";
			reg = <0 0x11e02000 0 0x1000>;
			#clock-cells = <1>;
		};

		imp_iic_wrap_en: syscon@11ec2000 {
			compatible = "mediatek,mt8188-imp_iic_wrap_en";
			reg = <0 0x11ec2000 0 0x1000>;
			#clock-cells = <1>;
		};

		vppsys0: syscon@14000000 {
			compatible = "mediatek,mt8188-vpp0", "syscon";
			reg = <0 0x14000000 0 0x1000>;
			#clock-cells = <1>;
		};

		vppsys1: syscon@14f00000 {
			compatible = "mediatek,mt8188-vpp1", "syscon";
			reg = <0 0x14f00000 0 0x1000>;
			#clock-cells = <1>;
		};

		vdosys0: syscon@1c01d000 {
			compatible = "mediatek,mt8188-vdo0", "syscon";
			reg = <0 0x1c01d000 0 0x1000>;
			#clock-cells = <1>;
		};

		vdosys1: syscon@1c100000 {
			compatible = "mediatek,mt8188-vdo1", "syscon";
			reg = <0 0x1c100000 0 0x1000>;
			#clock-cells = <1>;
		};
	};
};
