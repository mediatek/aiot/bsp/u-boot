// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2022 MediaTek Inc.
 * Author: Chris-QJ Chen <chris-qj.chen@mediatek.com>
 */

#include <common.h>
#include <dm.h>
#include <efi_loader.h>
#include <iot_ab.h>
#include <net.h>
#include <asm/io.h>
#include <linux/kernel.h>
#include <linux/arm-smccc.h>
#include <env.h>
#include <splash.h>

#include "mtk_panel.h"
#include "mt8390_evk.h"

#define MT8390_UPDATABLE_IMAGES	5

#if CONFIG_IS_ENABLED(EFI_HAVE_CAPSULE_SUPPORT)
static struct efi_fw_image fw_images[MT8390_UPDATABLE_IMAGES] = {0};

struct efi_capsule_update_info update_info = {
#if IS_ENABLED(CONFIG_MEDIATEK_IOT_AB_BOOT_SUPPORT)
#if (IS_ENABLED(CONFIG_DFU_MTD))
	.dfu_string = "mtd nor0=bl2.img part 1;"
			"fip.bin part %d;firmware.vfat part %d;u-boot-env.bin part 9",
#else
	.dfu_string = "mmc 0=bl2.img raw 0x0 0x2000 mmcpart 1;"
			"fip.bin part 0 %d;firmware.vfat part 0 %d;u-boot-env.bin raw 0x0 0x2000 mmcpart 2",
#endif
#else
#if (IS_ENABLED(CONFIG_DFU_MTD))
	.dfu_string = "mtd nor0=bl2.img part 1;"
			"fip.bin part 2;firmware.vfat part 4;u-boot-env.bin part 9",
#else
	.dfu_string = "mmc 0=bl2.img raw 0x0 0x2000 mmcpart 1;"
			"fip.bin part 0 1;firmware.vfat part 0 3;u-boot-env.bin raw 0x0 0x2000 mmcpart 2",
#endif
#endif
	.images = fw_images,
};

u8 num_image_type_guids = MT8390_UPDATABLE_IMAGES;
#endif

#if defined(CONFIG_EFI_HAVE_CAPSULE_SUPPORT) && defined(CONFIG_EFI_PARTITION)
enum mt8390_updatable_images {
	MT8390_BL2_IMAGE = 1,
	MT8390_FIP_IMAGE,
	MT8390_FW_IMAGE,
	MT8390_ENV_IMAGE,
	MT8390_FIT_IMAGE,
};

static bool board_is_genio_700_evk(void)
{
	return CONFIG_IS_ENABLED(TARGET_MT8188) &&
		of_machine_is_compatible("mediatek,genio-700-evk");
}

static bool board_is_genio_700_evk_qspi(void)
{
	return CONFIG_IS_ENABLED(TARGET_MT8188) &&
		of_machine_is_compatible("mediatek,genio-700-evk-qspi");
}

void mediatek_capsule_update_board_setup(void)
{
	fw_images[0].image_index = MT8390_FIT_IMAGE;
	fw_images[1].image_index = MT8390_FIP_IMAGE;
	fw_images[2].image_index = MT8390_BL2_IMAGE;
	fw_images[3].image_index = MT8390_FW_IMAGE;
	fw_images[4].image_index = MT8390_ENV_IMAGE;

	if (board_is_genio_700_evk()) {
		efi_guid_t image_type_guid = GENIO_700_EVK_FIT_IMAGE_GUID;
		efi_guid_t uboot_image_type_guid = GENIO_700_EVK_FIP_IMAGE_GUID;
		efi_guid_t bl2_image_type_guid = GENIO_700_EVK_BL2_IMAGE_GUID;
		efi_guid_t fw_image_type_guid = GENIO_700_EVK_FW_IMAGE_GUID;
		efi_guid_t env_image_type_guid = GENIO_700_EVK_ENV_IMAGE_GUID;

		guidcpy(&fw_images[0].image_type_id, &image_type_guid);
		guidcpy(&fw_images[1].image_type_id, &uboot_image_type_guid);
		guidcpy(&fw_images[2].image_type_id, &bl2_image_type_guid);
		guidcpy(&fw_images[3].image_type_id, &fw_image_type_guid);
		guidcpy(&fw_images[4].image_type_id, &env_image_type_guid);

		fw_images[0].fw_name = u"GENIO-700-EVK-FIT";
		fw_images[1].fw_name = u"GENIO-700-EVK-FIP";
		fw_images[2].fw_name = u"GENIO-700-EVK-BL2";
		fw_images[3].fw_name = u"GENIO-700-EVK-FW";
		fw_images[4].fw_name = u"GENIO-700-EVK-ENV";
	} else if (board_is_genio_700_evk_qspi()) {
		efi_guid_t image_type_guid = GENIO_700_EVK_QSPI_FIT_IMAGE_GUID;
		efi_guid_t uboot_image_type_guid = GENIO_700_EVK_QSPI_FIP_IMAGE_GUID;
		efi_guid_t bl2_image_type_guid = GENIO_700_EVK_QSPI_BL2_IMAGE_GUID;
		efi_guid_t fw_image_type_guid = GENIO_700_EVK_QSPI_FW_IMAGE_GUID;
		efi_guid_t env_image_type_guid = GENIO_700_EVK_QSPI_ENV_IMAGE_GUID;

		guidcpy(&fw_images[0].image_type_id, &image_type_guid);
		guidcpy(&fw_images[1].image_type_id, &uboot_image_type_guid);
		guidcpy(&fw_images[2].image_type_id, &bl2_image_type_guid);
		guidcpy(&fw_images[3].image_type_id, &fw_image_type_guid);
		guidcpy(&fw_images[4].image_type_id, &env_image_type_guid);

		fw_images[0].fw_name = u"GENIO-700-EVK-QSPI-FIT";
		fw_images[1].fw_name = u"GENIO-700-EVK-QSPI-FIP";
		fw_images[2].fw_name = u"GENIO-700-EVK-QSPI-BL2";
		fw_images[3].fw_name = u"GENIO-700-EVK-QSPI-FW";
		fw_images[4].fw_name = u"GENIO-700-EVK-QSPI-ENV";
	}
}

#if IS_ENABLED(CONFIG_MEDIATEK_IOT_AB_BOOT_SUPPORT)
void set_dfu_alt_info(char *interface, char *devstr)
{
	char alt[BOOTCTRL_DFU_ALT_LEN] = {0};
	const char *s = env_get(BOOTCTRL_ENV);

	if (s) {
		if (!strcmp(s, "a")) {
			if (sprintf(alt, update_info.dfu_string, BOOTCTRL_FIP_NUM + PART_BOOT_B,
				    BOOTCTRL_FW_NUM + PART_BOOT_B) < 0)
				return;
		} else if (!strcmp(s, "b")) {
			if (sprintf(alt, update_info.dfu_string,
				    BOOTCTRL_FIP_NUM, BOOTCTRL_FW_NUM) < 0)
				return;
		}
		env_set("dfu_alt_info", alt);
	}
}
#endif
#endif /* CONFIG_EFI_HAVE_CAPSULE_SUPPORT && CONFIG_EFI_PARTITION */

#if IS_ENABLED(CONFIG_SPLASH_SCREEN) && IS_ENABLED(CONFIG_SPI_FLASH)
static struct splash_location genio_700_evk_spi_splash_locations[] = {
	{
		.name = "sf",
		.storage = SPLASH_STORAGE_SF,
		.flags = SPLASH_STORAGE_RAW,
		.offset = 0x1580000,
	},
};

int splash_screen_prepare(void)
{
	return splash_source_load(genio_700_evk_spi_splash_locations,
				  ARRAY_SIZE(genio_700_evk_spi_splash_locations));
}
#endif

int board_init(void)
{
	struct udevice *dev;
	int ret;

	if (CONFIG_IS_ENABLED(USB_GADGET)) {
		ret = uclass_get_device(UCLASS_USB_GADGET_GENERIC, 0, &dev);
		if (ret) {
			pr_err("%s: Cannot find USB device\n", __func__);
			return ret;
		}
	}

	if (CONFIG_IS_ENABLED(USB_ETHER))
		usb_ether_init();

	printf("Enabling SCP SRAM\n");
	for (unsigned int val = 0xFFFFFFFF; val != 0U;) {
		val = val >> 1;
		writel(val, 0x1072102C);
	}

	if (IS_ENABLED(CONFIG_EFI_HAVE_CAPSULE_SUPPORT) &&
	    IS_ENABLED(CONFIG_EFI_PARTITION))
		mediatek_capsule_update_board_setup();

	return 0;
}

int check_board_id(void)
{
	unsigned int data = 0;

	adc_channel_single_shot(MT8390_ADC_NAME, MT8390_BOARD_ID_ADC_CHANNEL, &data);

	if (data > MT8390_P1V4_THRESH)
		return MT8390_EVK_BOARD_P1V4;
	return MT8390_EVK_BOARD_LEGACY;
}

int board_late_init(void)
{
	int board_id;
	char *fit_var_name = "boot_conf";
	char *efi_var_name = "list_dtbo";
	char *dtbo_val;
	char *dtbo_new_val;

	board_id = check_board_id();

	if (board_id == MT8390_EVK_BOARD_P1V4) {
		dtbo_val = env_get(fit_var_name);
		if (!strstr(dtbo_val, MT8390_P1V4_DSI_DTS)) {
			dtbo_new_val = (char *)malloc((strlen(dtbo_val) +
						      strlen(MT8390_P1V4_DSI_DTS) + 1));
			strcpy(dtbo_new_val, dtbo_val);
			strcat(dtbo_new_val, MT8390_P1V4_DSI_DTS);
			env_set(fit_var_name, dtbo_new_val);
			free(dtbo_new_val);
		}

		dtbo_val = env_get(efi_var_name);
		if (!strstr(dtbo_val, MT8390_P1V4_DSI_DTS_EFI)) {
			dtbo_new_val = (char *)malloc((strlen(dtbo_val) +
							   strlen(MT8390_P1V4_DSI_DTS_EFI) + 2));
			strcpy(dtbo_new_val, dtbo_val);
			strcat(strcat(dtbo_new_val, " "), MT8390_P1V4_DSI_DTS_EFI);
			env_set(efi_var_name, dtbo_new_val);
			free(dtbo_new_val);
		}
	}
	return 0;
}

void panel_get_desc(struct panel_description **panel_desc)
{
	int board_id;

	board_id = check_board_id();

	if (board_id == MT8390_EVK_BOARD_P1V4)
		panel_get_desc_kd070fhfid078(panel_desc);
	else
		panel_get_desc_kd070fhfid015(panel_desc);
}
